// types/plugin.ts
function applyMetadata(handler2, info) {
  handler2.info = info;
}

// plugins/label_filter/info.json
var info_default = {
  name: "label_filter",
  version: "1.0.0",
  authors: ["boi123212321"],
  description: "Filter labels returned by other plugins",
  events: [
    "actorCreated",
    "actorCustom",
    "sceneCreated",
    "sceneCustom",
    "studioCreated",
    "studioCustom"
  ],
  arguments: [
    {
      name: "whitelist",
      type: "String[]",
      required: false,
      default: [],
      description: "Labels to include"
    },
    {
      name: "blacklist",
      type: "String[]",
      required: false,
      default: [],
      description: "Labels to exclude"
    }
  ]
};

// plugins/label_filter/main.ts
var lower = (s) => s.toLowerCase();
var handler = async ({ args, data }) => {
  const whitelist = (args.whitelist || []).map(lower);
  const blacklist = (args.blacklist || []).map(lower);
  if (!data.labels) {
    return {};
  }
  if (!whitelist.length && !blacklist.length) {
    return {};
  }
  return {
    labels: data.labels.filter((label) => {
      const lowercased = lower(label);
      if (whitelist.length && !whitelist.includes(label)) {
        return false;
      }
      return blacklist.every((blacklisted) => blacklisted !== lowercased);
    })
  };
};
handler.requiredVersion = "0.30.0-rc.3 - 1";
applyMetadata(handler, info_default);
var main_default = handler;
export {
  main_default as default
};
