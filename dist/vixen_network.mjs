// plugins/vixen_network/main.ts
import { basename, extname } from "node:path";

// types/plugin.ts
function applyMetadata(handler2, info) {
  handler2.info = info;
}

// plugins/vixen_network/info.json
var info_default = {
  name: "vixen_network",
  version: "1.1.0",
  authors: ["boi12321"],
  description: "Scrape VIXEN sites",
  events: ["sceneCreated", "sceneCustom"],
  arguments: [
    {
      name: "stripString",
      type: "String",
      required: false,
      default: "[^a-zA-Z0-9'/\\,()[\\]{}-]",
      description: "Matcher string regex"
    },
    {
      name: "dry",
      type: "Boolean",
      required: false,
      default: false,
      description: "Whether to commit data changes"
    },
    {
      name: "useThumbnail",
      type: "Boolean",
      required: false,
      default: false,
      description: "Download & attach scene thumbnail"
    },
    {
      name: "useChapters",
      type: "Boolean",
      required: false,
      default: false,
      description: "Create scene markers from chapters"
    }
  ]
};

// plugins/vixen_network/main.ts
var sites = [
  {
    name: "BLACKED RAW",
    url: "https://www.blackedraw.com"
  },
  {
    name: "BLACKED",
    url: "https://www.blacked.com"
  },
  {
    name: "TUSHY RAW",
    url: "https://www.tushyraw.com"
  },
  {
    name: "TUSHY",
    url: "https://www.tushy.com"
  },
  {
    name: "VIXEN",
    url: "https://www.vixen.com"
  },
  {
    name: "DEEPER",
    url: "https://www.deeper.com"
  },
  {
    name: "SLAYED",
    url: "https://www.slayed.com"
  }
];
var sceneFragment = `title
site
slug
description
releaseDate
categories {
  name
}
chapters {
  video {
    title
    seconds
  }
}
models {
  name
}
images {
  poster {
    ...ImageInfo
  }
}`;
var findSceneGraphqlQuery = `
query($slug: String!, $site: Site!) {
  findOneVideo(input: { slug: $slug, site: $site }) {
    ${sceneFragment}
  }
}

fragment ImageInfo on Image {
  src
  highdpi {
    double
  }
}
`;
var searchSceneGraphqlQuery = `
query($query: String!, $site: Site!) {
  searchVideos(input: { query: $query, site: $site }) {
    edges {
      node {
        ${sceneFragment}
      }
    }
  }
}

fragment ImageInfo on Image {
  src
  highdpi {
    double
  }
}
`.trim();
async function findScene(ctx, id) {
  const [site, slug] = id.split(":");
  const baseUrl = `https://www.${site}.com`;
  const url = `${baseUrl}/graphql`;
  ctx.$logger.debug(`POST ${url} with id "${id}"`);
  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify({
      query: findSceneGraphqlQuery,
      variables: {
        slug,
        site: site.toUpperCase()
      }
    }),
    headers: {
      "Content-Type": "application/json",
      "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0",
      referer: baseUrl,
      origin: baseUrl
    }
  });
  if (!response.ok) {
    throw new Error(`Request failed (${response.status}): ${await response.text()}`);
  }
  const { data } = await response.json();
  return data.findOneVideo;
}
async function searchScenes(ctx, site, query) {
  const url = `${site.url}/graphql`;
  ctx.$logger.debug(`POST ${url} with query "${query}"`);
  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify({
      query: searchSceneGraphqlQuery,
      variables: {
        query,
        site: site.name.replace(/ /g, "")
      }
    }),
    headers: {
      "Content-Type": "application/json",
      "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0",
      referer: site.url,
      origin: site.url
    }
  });
  if (!response.ok) {
    throw new Error(`Request failed (${response.status}): ${await response.text()}`);
  }
  const { data } = await response.json();
  return data.searchVideos.edges.map(({ node }) => node);
}
function getArgs(ctx) {
  return ctx.args;
}
function basicMatch(ctx, a, b) {
  const stripString = getArgs(ctx).stripString || "[^a-zA-Z0-9'/\\,()[\\]{}-]";
  const stripRegex = new RegExp(stripString, "g");
  function normalize(str) {
    return str.trim().toLocaleLowerCase().replace(stripRegex, "");
  }
  return normalize(a).includes(normalize(b));
}
function findSite(ctx, str) {
  return sites.find((site) => {
    ctx.$logger.debug(`Compare "${str}" <-> "${site.name}"`);
    return basicMatch(ctx, str, site.name);
  });
}
async function searchByTerms(ctx, site, terms) {
  const { scene, $logger, $formatMessage } = ctx;
  const bname = basename(scene.path);
  const filename = bname.replace(extname(bname), "");
  for (const term of terms) {
    $logger.verbose(`Searching ${site.name} with query "${term}"`);
    const searchResults = await searchScenes(ctx, site, term);
    $logger.debug("Search results:");
    $logger.debug($formatMessage(searchResults.map(({ title }) => title)));
    const found = searchResults.filter(({ title }) => basicMatch(ctx, filename, title)).sort((a, b) => b.title.length - a.title.length).at(0);
    if (!found) {
      $logger.warn(`No result found for "${site.url}" with search term "${term}"`);
    } else {
      return found;
    }
  }
  return null;
}
var VIXEN_ID_CUSTOM_FIELD = "Vixen ID";
var SCENE_ID_CUSTOM_FIELD = "Scene ID";
var handler = async (ctx) => {
  const { scene, sceneName, event, $logger, $formatMessage } = ctx;
  const result = {
    custom: {},
    $markers: []
  };
  const customFields = await ctx.$getCustomFields();
  const vixenIdCustomField = customFields.find(
    (x) => x.name.toLowerCase() === VIXEN_ID_CUSTOM_FIELD.toLowerCase() || x.name.toLowerCase() === SCENE_ID_CUSTOM_FIELD.toLowerCase()
  );
  let found = null;
  if (vixenIdCustomField) {
    if (!vixenIdCustomField.target.includes("SCENES" /* SCENES */)) {
      ctx.$throw(
        `Vixen ID custom field is misconfigured, should have custom field target "Scenes"`
      );
    }
    if (vixenIdCustomField.type !== "STRING" /* STRING */) {
      ctx.$throw(`Vixen ID custom field is misconfigured, should have type "String"`);
    }
    const existingId = scene.customFields[vixenIdCustomField._id];
    if (existingId) {
      $logger.info(`Using scene ID from custom fields: "${existingId}"`);
      found = await findScene(ctx, existingId);
    }
  }
  if (!found) {
    if (!scene?.path) {
      $logger.error(`No scene path: ${scene._id}`);
      return {};
    }
    $logger.verbose(`Checking VIXEN sites for "${scene.path}"`);
    const site = findSite(ctx, scene.path) || await (async () => {
      const studio = await ctx.$getStudio();
      if (!studio) {
        return null;
      }
      return findSite(ctx, studio.name);
    })();
    if (!site) {
      $logger.warn(`No VIXEN site found in "${scene.path}"`);
      return {};
    }
    found = await searchByTerms(ctx, site, [
      // Use plugin result name if it exists
      ...ctx.data.name ? [ctx.data.name] : [],
      // Search by scene name
      sceneName,
      // Fallback to actor names as search query
      ...(await ctx.$getActors()).map(({ name }) => name)
    ]);
    if (!found) {
      $logger.warn(`No scene found on ${site.name}`);
      return {};
    }
  }
  const sceneId = `${found.site}:${found.slug}`;
  $logger.info(`Using scene "${found.title}" (${sceneId})`);
  result.custom[VIXEN_ID_CUSTOM_FIELD] = sceneId;
  result.custom[SCENE_ID_CUSTOM_FIELD] = sceneId;
  result.name = found.title;
  result.actors = found.models.map(({ name }) => name).sort();
  result.description = found.description;
  result.studio = found.site;
  result.releaseDate = new Date(found.releaseDate).valueOf();
  result.labels = found.categories.map(({ name }) => name).sort();
  const thumbUrl = found.images.poster.reverse().find((x) => x.src);
  result.$thumbnail = thumbUrl;
  const args = getArgs(ctx);
  if (args.useThumbnail && thumbUrl) {
    $logger.verbose("Setting thumbnail");
    result.thumbnail = await ctx.$createImage(thumbUrl.src, `${result.name}`, true);
  }
  if (args.useChapters) {
    const chapters = found.chapters.video;
    for (const { title, seconds } of chapters) {
      result.$markers.push({
        name: title,
        time: seconds
      });
    }
  }
  if (args.dry) {
    $logger.info(`Would have returned ${$formatMessage(result)}`);
    return {};
  }
  $logger.verbose(`Creating ${result.$markers.length} markers`);
  for (const { name, time } of result.$markers) {
    $logger.silly(`Creating marker: ${name} at ${time}s`);
    await ctx.$createMarker(name, time);
  }
  return result;
};
handler.requiredVersion = "0.30.0-rc.3 - 1";
applyMetadata(handler, info_default);
var main_default = handler;
export {
  main_default as default
};
