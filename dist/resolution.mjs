// types/plugin.ts
function applyMetadata(handler2, info) {
  handler2.info = info;
}

// plugins/resolution/info.json
var info_default = {
  name: "resolution",
  version: "1.0.0",
  authors: ["boi123212321"],
  description: "Add resolution labels to a scene",
  events: ["sceneCreated", "sceneCustom"],
  arguments: [
    {
      name: "resolutions",
      type: "number[]",
      required: false,
      default: [],
      description: "Resolutions to match against the scene's path, when the scene's metadata has not yet been extracted"
    }
  ]
};

// plugins/resolution/main.ts
var handler = async (ctx) => {
  const { args, $throw, data, scenePath, scene } = ctx;
  if (!scenePath) {
    throw $throw("Uh oh. You shouldn't use the plugin for this type of event");
  }
  let resolutions = [144, 240, 360, 480, 720, 1080, 2160];
  if (args.resolutions) {
    if (!Array.isArray(args.resolutions) || args.resolutions.some((x) => typeof x !== "number")) {
      throw $throw("Invalid resolutions array");
    }
    resolutions = args.resolutions.map(Math.floor);
  }
  if (scene.meta.dimensions && scene.meta.dimensions.height) {
    const formattedResolution = `${scene.meta.dimensions.height}p`;
    if (data.labels) {
      return {
        labels: [...data.labels, formattedResolution]
      };
    }
    return {
      labels: [formattedResolution]
    };
  }
  const resolution = resolutions.find((res) => scenePath.toLowerCase().includes(`${res}p`));
  if (resolution) {
    const formattedResolution = `${resolution}p`;
    if (data.labels) {
      return {
        labels: [...data.labels, formattedResolution]
      };
    }
    return {
      labels: [formattedResolution]
    };
  }
  return {};
};
handler.requiredVersion = "0.30.0-rc.3 - 1";
applyMetadata(handler, info_default);
var main_default = handler;
export {
  main_default as default
};
