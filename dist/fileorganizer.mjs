var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));

// node_modules/.pnpm/dayjs@1.11.9/node_modules/dayjs/dayjs.min.js
var require_dayjs_min = __commonJS({
  "node_modules/.pnpm/dayjs@1.11.9/node_modules/dayjs/dayjs.min.js"(exports, module) {
    !function(t, e) {
      "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : (t = "undefined" != typeof globalThis ? globalThis : t || self).dayjs = e();
    }(exports, function() {
      "use strict";
      var t = 1e3, e = 6e4, n = 36e5, r = "millisecond", i = "second", s = "minute", u = "hour", a = "day", o = "week", c = "month", f = "quarter", h = "year", d = "date", l = "Invalid Date", $ = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/, y = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g, M = { name: "en", weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"), ordinal: function(t2) {
        var e2 = ["th", "st", "nd", "rd"], n2 = t2 % 100;
        return "[" + t2 + (e2[(n2 - 20) % 10] || e2[n2] || e2[0]) + "]";
      } }, m = function(t2, e2, n2) {
        var r2 = String(t2);
        return !r2 || r2.length >= e2 ? t2 : "" + Array(e2 + 1 - r2.length).join(n2) + t2;
      }, v = { s: m, z: function(t2) {
        var e2 = -t2.utcOffset(), n2 = Math.abs(e2), r2 = Math.floor(n2 / 60), i2 = n2 % 60;
        return (e2 <= 0 ? "+" : "-") + m(r2, 2, "0") + ":" + m(i2, 2, "0");
      }, m: function t2(e2, n2) {
        if (e2.date() < n2.date()) return -t2(n2, e2);
        var r2 = 12 * (n2.year() - e2.year()) + (n2.month() - e2.month()), i2 = e2.clone().add(r2, c), s2 = n2 - i2 < 0, u2 = e2.clone().add(r2 + (s2 ? -1 : 1), c);
        return +(-(r2 + (n2 - i2) / (s2 ? i2 - u2 : u2 - i2)) || 0);
      }, a: function(t2) {
        return t2 < 0 ? Math.ceil(t2) || 0 : Math.floor(t2);
      }, p: function(t2) {
        return { M: c, y: h, w: o, d: a, D: d, h: u, m: s, s: i, ms: r, Q: f }[t2] || String(t2 || "").toLowerCase().replace(/s$/, "");
      }, u: function(t2) {
        return void 0 === t2;
      } }, g = "en", D = {};
      D[g] = M;
      var p = function(t2) {
        return t2 instanceof b;
      }, S = function t2(e2, n2, r2) {
        var i2;
        if (!e2) return g;
        if ("string" == typeof e2) {
          var s2 = e2.toLowerCase();
          D[s2] && (i2 = s2), n2 && (D[s2] = n2, i2 = s2);
          var u2 = e2.split("-");
          if (!i2 && u2.length > 1) return t2(u2[0]);
        } else {
          var a2 = e2.name;
          D[a2] = e2, i2 = a2;
        }
        return !r2 && i2 && (g = i2), i2 || !r2 && g;
      }, w = function(t2, e2) {
        if (p(t2)) return t2.clone();
        var n2 = "object" == typeof e2 ? e2 : {};
        return n2.date = t2, n2.args = arguments, new b(n2);
      }, O = v;
      O.l = S, O.i = p, O.w = function(t2, e2) {
        return w(t2, { locale: e2.$L, utc: e2.$u, x: e2.$x, $offset: e2.$offset });
      };
      var b = function() {
        function M2(t2) {
          this.$L = S(t2.locale, null, true), this.parse(t2);
        }
        var m2 = M2.prototype;
        return m2.parse = function(t2) {
          this.$d = function(t3) {
            var e2 = t3.date, n2 = t3.utc;
            if (null === e2) return /* @__PURE__ */ new Date(NaN);
            if (O.u(e2)) return /* @__PURE__ */ new Date();
            if (e2 instanceof Date) return new Date(e2);
            if ("string" == typeof e2 && !/Z$/i.test(e2)) {
              var r2 = e2.match($);
              if (r2) {
                var i2 = r2[2] - 1 || 0, s2 = (r2[7] || "0").substring(0, 3);
                return n2 ? new Date(Date.UTC(r2[1], i2, r2[3] || 1, r2[4] || 0, r2[5] || 0, r2[6] || 0, s2)) : new Date(r2[1], i2, r2[3] || 1, r2[4] || 0, r2[5] || 0, r2[6] || 0, s2);
              }
            }
            return new Date(e2);
          }(t2), this.$x = t2.x || {}, this.init();
        }, m2.init = function() {
          var t2 = this.$d;
          this.$y = t2.getFullYear(), this.$M = t2.getMonth(), this.$D = t2.getDate(), this.$W = t2.getDay(), this.$H = t2.getHours(), this.$m = t2.getMinutes(), this.$s = t2.getSeconds(), this.$ms = t2.getMilliseconds();
        }, m2.$utils = function() {
          return O;
        }, m2.isValid = function() {
          return !(this.$d.toString() === l);
        }, m2.isSame = function(t2, e2) {
          var n2 = w(t2);
          return this.startOf(e2) <= n2 && n2 <= this.endOf(e2);
        }, m2.isAfter = function(t2, e2) {
          return w(t2) < this.startOf(e2);
        }, m2.isBefore = function(t2, e2) {
          return this.endOf(e2) < w(t2);
        }, m2.$g = function(t2, e2, n2) {
          return O.u(t2) ? this[e2] : this.set(n2, t2);
        }, m2.unix = function() {
          return Math.floor(this.valueOf() / 1e3);
        }, m2.valueOf = function() {
          return this.$d.getTime();
        }, m2.startOf = function(t2, e2) {
          var n2 = this, r2 = !!O.u(e2) || e2, f2 = O.p(t2), l2 = function(t3, e3) {
            var i2 = O.w(n2.$u ? Date.UTC(n2.$y, e3, t3) : new Date(n2.$y, e3, t3), n2);
            return r2 ? i2 : i2.endOf(a);
          }, $2 = function(t3, e3) {
            return O.w(n2.toDate()[t3].apply(n2.toDate("s"), (r2 ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(e3)), n2);
          }, y2 = this.$W, M3 = this.$M, m3 = this.$D, v2 = "set" + (this.$u ? "UTC" : "");
          switch (f2) {
            case h:
              return r2 ? l2(1, 0) : l2(31, 11);
            case c:
              return r2 ? l2(1, M3) : l2(0, M3 + 1);
            case o:
              var g2 = this.$locale().weekStart || 0, D2 = (y2 < g2 ? y2 + 7 : y2) - g2;
              return l2(r2 ? m3 - D2 : m3 + (6 - D2), M3);
            case a:
            case d:
              return $2(v2 + "Hours", 0);
            case u:
              return $2(v2 + "Minutes", 1);
            case s:
              return $2(v2 + "Seconds", 2);
            case i:
              return $2(v2 + "Milliseconds", 3);
            default:
              return this.clone();
          }
        }, m2.endOf = function(t2) {
          return this.startOf(t2, false);
        }, m2.$set = function(t2, e2) {
          var n2, o2 = O.p(t2), f2 = "set" + (this.$u ? "UTC" : ""), l2 = (n2 = {}, n2[a] = f2 + "Date", n2[d] = f2 + "Date", n2[c] = f2 + "Month", n2[h] = f2 + "FullYear", n2[u] = f2 + "Hours", n2[s] = f2 + "Minutes", n2[i] = f2 + "Seconds", n2[r] = f2 + "Milliseconds", n2)[o2], $2 = o2 === a ? this.$D + (e2 - this.$W) : e2;
          if (o2 === c || o2 === h) {
            var y2 = this.clone().set(d, 1);
            y2.$d[l2]($2), y2.init(), this.$d = y2.set(d, Math.min(this.$D, y2.daysInMonth())).$d;
          } else l2 && this.$d[l2]($2);
          return this.init(), this;
        }, m2.set = function(t2, e2) {
          return this.clone().$set(t2, e2);
        }, m2.get = function(t2) {
          return this[O.p(t2)]();
        }, m2.add = function(r2, f2) {
          var d2, l2 = this;
          r2 = Number(r2);
          var $2 = O.p(f2), y2 = function(t2) {
            var e2 = w(l2);
            return O.w(e2.date(e2.date() + Math.round(t2 * r2)), l2);
          };
          if ($2 === c) return this.set(c, this.$M + r2);
          if ($2 === h) return this.set(h, this.$y + r2);
          if ($2 === a) return y2(1);
          if ($2 === o) return y2(7);
          var M3 = (d2 = {}, d2[s] = e, d2[u] = n, d2[i] = t, d2)[$2] || 1, m3 = this.$d.getTime() + r2 * M3;
          return O.w(m3, this);
        }, m2.subtract = function(t2, e2) {
          return this.add(-1 * t2, e2);
        }, m2.format = function(t2) {
          var e2 = this, n2 = this.$locale();
          if (!this.isValid()) return n2.invalidDate || l;
          var r2 = t2 || "YYYY-MM-DDTHH:mm:ssZ", i2 = O.z(this), s2 = this.$H, u2 = this.$m, a2 = this.$M, o2 = n2.weekdays, c2 = n2.months, f2 = n2.meridiem, h2 = function(t3, n3, i3, s3) {
            return t3 && (t3[n3] || t3(e2, r2)) || i3[n3].slice(0, s3);
          }, d2 = function(t3) {
            return O.s(s2 % 12 || 12, t3, "0");
          }, $2 = f2 || function(t3, e3, n3) {
            var r3 = t3 < 12 ? "AM" : "PM";
            return n3 ? r3.toLowerCase() : r3;
          };
          return r2.replace(y, function(t3, r3) {
            return r3 || function(t4) {
              switch (t4) {
                case "YY":
                  return String(e2.$y).slice(-2);
                case "YYYY":
                  return O.s(e2.$y, 4, "0");
                case "M":
                  return a2 + 1;
                case "MM":
                  return O.s(a2 + 1, 2, "0");
                case "MMM":
                  return h2(n2.monthsShort, a2, c2, 3);
                case "MMMM":
                  return h2(c2, a2);
                case "D":
                  return e2.$D;
                case "DD":
                  return O.s(e2.$D, 2, "0");
                case "d":
                  return String(e2.$W);
                case "dd":
                  return h2(n2.weekdaysMin, e2.$W, o2, 2);
                case "ddd":
                  return h2(n2.weekdaysShort, e2.$W, o2, 3);
                case "dddd":
                  return o2[e2.$W];
                case "H":
                  return String(s2);
                case "HH":
                  return O.s(s2, 2, "0");
                case "h":
                  return d2(1);
                case "hh":
                  return d2(2);
                case "a":
                  return $2(s2, u2, true);
                case "A":
                  return $2(s2, u2, false);
                case "m":
                  return String(u2);
                case "mm":
                  return O.s(u2, 2, "0");
                case "s":
                  return String(e2.$s);
                case "ss":
                  return O.s(e2.$s, 2, "0");
                case "SSS":
                  return O.s(e2.$ms, 3, "0");
                case "Z":
                  return i2;
              }
              return null;
            }(t3) || i2.replace(":", "");
          });
        }, m2.utcOffset = function() {
          return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);
        }, m2.diff = function(r2, d2, l2) {
          var $2, y2 = this, M3 = O.p(d2), m3 = w(r2), v2 = (m3.utcOffset() - this.utcOffset()) * e, g2 = this - m3, D2 = function() {
            return O.m(y2, m3);
          };
          switch (M3) {
            case h:
              $2 = D2() / 12;
              break;
            case c:
              $2 = D2();
              break;
            case f:
              $2 = D2() / 3;
              break;
            case o:
              $2 = (g2 - v2) / 6048e5;
              break;
            case a:
              $2 = (g2 - v2) / 864e5;
              break;
            case u:
              $2 = g2 / n;
              break;
            case s:
              $2 = g2 / e;
              break;
            case i:
              $2 = g2 / t;
              break;
            default:
              $2 = g2;
          }
          return l2 ? $2 : O.a($2);
        }, m2.daysInMonth = function() {
          return this.endOf(c).$D;
        }, m2.$locale = function() {
          return D[this.$L];
        }, m2.locale = function(t2, e2) {
          if (!t2) return this.$L;
          var n2 = this.clone(), r2 = S(t2, e2, true);
          return r2 && (n2.$L = r2), n2;
        }, m2.clone = function() {
          return O.w(this.$d, this);
        }, m2.toDate = function() {
          return new Date(this.valueOf());
        }, m2.toJSON = function() {
          return this.isValid() ? this.toISOString() : null;
        }, m2.toISOString = function() {
          return this.$d.toISOString();
        }, m2.toString = function() {
          return this.$d.toUTCString();
        }, M2;
      }(), _ = b.prototype;
      return w.prototype = _, [["$ms", r], ["$s", i], ["$m", s], ["$H", u], ["$W", a], ["$M", c], ["$y", h], ["$D", d]].forEach(function(t2) {
        _[t2[1]] = function(e2) {
          return this.$g(e2, t2[0], t2[1]);
        };
      }), w.extend = function(t2, e2) {
        return t2.$i || (t2(e2, b, w), t2.$i = true), w;
      }, w.locale = S, w.isDayjs = p, w.unix = function(t2) {
        return w(1e3 * t2);
      }, w.en = D[g], w.Ls = D, w.p = {}, w;
    });
  }
});

// types/plugin.ts
function applyMetadata(handler2, info) {
  handler2.info = info;
}

// plugins/fileorganizer/main.ts
import $fs from "node:fs";
import $path from "node:path";
import { promises as $fsPromises } from "node:fs";

// plugins/fileorganizer/template.ts
var import_dayjs = __toESM(require_dayjs_min());
var FIELD_NAME = "name";
var FIELD_RELEASE_DATE = "releaseDate";
var FIELD_RATING = "rating";
var FIELD_VIDEO_HEIGHT = "videoHeight";
var FIELD_VIDEO_WIDTH = "videoWidth";
var FIELD_VIDEO_DURATION = "videoDuration";
var FIELD_ACTORS = "actors";
var FIELD_STUDIO = "studio";
var FIELD_MOVIES = "movies";
var FIELD_LABELS = "labels";
var safeInitialDataForSceneCreate = ["videoDuration", "videoWidth", "videoHeight"];
function getTemplateMatcher() {
  return /{(?<prefix>[^{}<]*)<(?<field>[^\d\s\W(>]*)(?<args>[\d\W]*|(?:\([^){}]*\))*)>(?<suffix>[^{}]*)}/g;
}
function getTemplateFieldsResolvers(ctx) {
  const { args, scene, data } = ctx;
  const HAS_NAME_PROP = true;
  const NO_NAME_PROP = false;
  return [
    {
      name: FIELD_NAME,
      getPluginData: async (i) => data.name,
      getInitialData: async (i) => scene.name
    },
    {
      name: FIELD_RELEASE_DATE,
      getPluginData: async (i) => data.releaseDate ? (0, import_dayjs.default)(data.releaseDate).format(args.dateFormat) : void 0,
      getInitialData: async (i) => scene.releaseDate ? (0, import_dayjs.default)(scene.releaseDate).format(args.dateFormat) : void 0
    },
    {
      name: FIELD_RATING,
      getPluginData: async (i) => data.rating?.toString(),
      getInitialData: async (i) => scene.rating?.toString()
    },
    {
      name: FIELD_VIDEO_HEIGHT,
      getInitialData: async (i) => scene.meta?.dimensions?.height?.toString()
    },
    {
      name: FIELD_VIDEO_WIDTH,
      getInitialData: async (i) => scene.meta?.dimensions?.width?.toString()
    },
    {
      name: FIELD_VIDEO_DURATION,
      getInitialData: async (i) => formatVideoDuration(scene.meta?.duration)
    },
    {
      name: FIELD_ACTORS,
      getPluginData: async (i) => {
        const actors = data.actors;
        return arrayToString(ctx, actors, i, NO_NAME_PROP);
      },
      getInitialData: async (i) => {
        const actors = await ctx.$getActors();
        return arrayToString(ctx, actors, i, HAS_NAME_PROP);
      }
    },
    {
      name: FIELD_STUDIO,
      getPluginData: async (i) => data.studio,
      getInitialData: async (i) => {
        return (await ctx.$getStudio())?.name;
      }
    },
    {
      name: FIELD_LABELS,
      getPluginData: async (i) => {
        const labels = data.labels;
        return arrayToString(ctx, labels, i, NO_NAME_PROP);
      },
      getInitialData: async (i) => {
        const labels = await ctx.$getLabels();
        return arrayToString(ctx, labels, i, HAS_NAME_PROP);
      }
    },
    {
      name: FIELD_MOVIES,
      getPluginData: async (i) => data.movie,
      getInitialData: async (i) => {
        const movies = await ctx.$getMovies();
        return arrayToString(ctx, movies, i, HAS_NAME_PROP);
      }
    }
  ];
}
function getAndValidateFieldArgs(args) {
  if (!args) {
    return { isValid: true, isMandatory: false };
  }
  let isValid = false;
  let isMandatory = false;
  let index;
  const matches = args.matchAll(/^(?<index>\d{0,2})(?<mandatory>!?)$/gm);
  for (const match of matches) {
    if (match.groups?.index) {
      index = parseInt(match.groups?.index) - 1;
    }
    if (match.groups?.mandatory) {
      isMandatory = true;
    }
    isValid = true;
  }
  return { isValid, isMandatory, index };
}
async function getTemplateFieldValue(ctx, resolver, index) {
  let fieldValue;
  if (resolver.getPluginData) {
    fieldValue = await resolver.getPluginData(index);
  }
  if (ctx.event === "sceneCustom" || ctx.event === "sceneCreated" && safeInitialDataForSceneCreate.includes(resolver.name)) {
    fieldValue ??= await resolver.getInitialData(index);
  }
  return fieldValue ?? null;
}
function arrayToString(ctx, array, index, hasNameProperty) {
  if (!array || !array.length) {
    return;
  }
  const i = index || -1;
  const a = hasNameProperty ? array.map((a2) => a2.name) : array;
  if (i < 0) {
    return a.join(ctx.args.multiValuesSeparator);
  }
  if (i >= a.length) {
    ctx.$logger.verbose(
      `The field <${(typeof a).toLowerCase()}${i + 1}> from template is out of bounds (total items: ${a.length})`
    );
    return;
  }
  return a[i];
}
function formatVideoDuration(duration) {
  if (duration) {
    return (0, import_dayjs.default)().startOf("day").add(duration, "seconds").format(duration < 3600 ? "mm\u2236ss" : "H\u2236mm\u2236ss");
  }
  return null;
}

// plugins/fileorganizer/utils.ts
function toNormalizedSafeFilename(ctx, unsafeName) {
  let safeFileName = sanitize(unsafeName, ctx.args.characterReplacement);
  if (ctx.args.normalizeAccents) {
    safeFileName = safeFileName.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }
  if (ctx.args.normalizeMultipleSpaces) {
    safeFileName = safeFileName.replace(/ {2,}/g, " ");
  }
  return safeFileName.trim();
}
function sanitize(input, replacement) {
  const illegalRe = /[/?<>\\:*|"]/g;
  const controlRe = /[\x00-\x1f\x80-\x9f]/g;
  const reservedRe = /^\.+$/;
  const windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
  const windowsTrailingRe = /[. ]+$/;
  if (typeof input !== "string") {
    throw new Error("Input must be string");
  }
  let sanitized = input;
  if (replacement) {
    replacement.forEach((e) => {
      sanitized = sanitized.replace(e.original, e.replacement);
    });
  }
  sanitized = sanitized.replace(illegalRe, "").replace(controlRe, "").replace(reservedRe, "").replace(windowsReservedRe, "").replace(windowsTrailingRe, "");
  return sanitized;
}

// plugins/fileorganizer/info.json
var info_default = {
  name: "fileorganizer",
  version: "1.0.0",
  authors: ["arcadianCdr"],
  description: "Use your custom-defined templates to rename your scene files.",
  events: ["sceneCreated", "sceneCustom"],
  arguments: [
    {
      name: "dry",
      type: "Boolean",
      required: false,
      default: false,
      description: "Whether to perform the rename operation or just a simulation."
    },
    {
      name: "fileStructureTemplate",
      type: "String",
      required: true,
      default: "",
      description: "The template for the new name. See documentation above for details."
    },
    {
      name: "normalizeAccents",
      type: "Boolean",
      required: false,
      default: false,
      description: "Whether to normalize file names and path to unaccented unicode."
    },
    {
      name: "normalizeMultipleSpaces",
      type: "Boolean",
      required: false,
      default: true,
      description: "Whether to replace multiple spaces with a single space."
    },
    {
      name: "nameConflictHandling",
      type: "String",
      required: false,
      default: "rename",
      description: "Behavior in case of name conflicts. Possible values are: `rename` and `skip`. With `rename`, the new filename is suffixed with a number so that it does not conflict with an existing name anymore. With `skip`, the rename operation is cancelled."
    },
    {
      name: "dateFormat",
      type: "String",
      required: false,
      default: "YYYY-MM-DD",
      description: "The date format to use in file names. The full details are available at https://day.js.org/docs/en/display/format although you probably just need `YYYY`, `MM` and `DD`."
    },
    {
      name: "multiValuesSeparator",
      type: "String",
      required: true,
      default: ", ",
      description: "The separator to use for multiple values (like actors, labels,...). For instance, with a `', '` as separator, a list of 3 labels will look like: `label1, label2, label3`."
    },
    {
      name: "characterReplacement",
      type: "object[]",
      required: false,
      default: [{ original: ":", replacement: "\u2236" }],
      description: "Used to substitute characters with a replacement alternative. See doc above for details. Note: the examples below looks like it is replacing a colon by a colon, but it is actually replacing the colon (illegal in filenames) by the similar looking 'mathematical ratio' character (allowed in filenames)"
    }
  ]
};

// plugins/fileorganizer/main.ts
async function filenameMaker(ctx, template) {
  const { $logger } = ctx;
  let result = "";
  const fieldResolvers = getTemplateFieldsResolvers(ctx);
  const matches = template.matchAll(getTemplateMatcher());
  for (const match of matches) {
    const resolver = fieldResolvers.find(
      (item) => item.name.toLowerCase() === match.groups?.field?.toLowerCase()
    );
    const fieldArgs = getAndValidateFieldArgs(match.groups?.args);
    if (!resolver || !fieldArgs.isValid) {
      $logger.error(
        `Unsupported field ${match.groups?.field} (or its arguments) in template ${template}`
      );
      return null;
    }
    const fieldValue = await getTemplateFieldValue(ctx, resolver, fieldArgs.index);
    if (fieldArgs.isMandatory && !fieldValue) {
      $logger.info(`Skipping rename (the mandatory field ${resolver.name} has no value)`);
      return null;
    }
    if (fieldValue) {
      const groupOutput = `${match.groups?.prefix}${fieldValue}${match.groups?.suffix}`;
      result += groupOutput;
      $logger.debug(`Group output for field <${resolver.name}>: '${groupOutput}'`);
    } else {
      $logger.debug(`Got no value for field <${resolver.name}>: the whole group is skipped.`);
    }
  }
  if (result) {
    return toNormalizedSafeFilename(ctx, result);
  } else {
    $logger.warn(
      `Could not generate a new filename based on template: '${template}'. All the template fields were without value.`
    );
    return null;
  }
}
var ConflictAction = /* @__PURE__ */ ((ConflictAction2) => {
  ConflictAction2["RENAME"] = "rename";
  ConflictAction2["SKIP"] = "skip";
  return ConflictAction2;
})(ConflictAction || {});
var handler = async (ctx) => {
  const { args, scenePath, $formatMessage, $logger, $throw } = ctx;
  if (!["sceneCreated", "sceneCustom"].includes(ctx.event)) {
    $throw("Uh oh. You shouldn't use the plugin for this type of event");
  }
  $logger.verbose(`Starting fileorganizer for scene: '${scenePath}'`);
  if (!args.fileStructureTemplate || !getTemplateMatcher().test(args.fileStructureTemplate)) {
    $throw(`invalid teamplate: '${args.fileStructureTemplate}'. Please correct and retry.`);
  }
  args.dateFormat ??= args.dateFormat = "YYYY-MM-DD";
  args.characterReplacement ??= [{ original: ":", replacement: "\u2236" }];
  args.multiValuesSeparator ??= ", ";
  args.nameConflictHandling ??= "rename" /* RENAME */;
  args.normalizeAccents ??= false;
  args.normalizeMultipleSpaces ??= true;
  if (!Object.values(ConflictAction).includes(args.nameConflictHandling)) {
    $throw(
      `Unsupported 'nameConflictHandling' argument value: ${args.nameConflictHandling}. Please adapt your config and retry.`
    );
  }
  const newFileName = await filenameMaker(ctx, args.fileStructureTemplate);
  if (!newFileName) {
    return {};
  }
  if (newFileName.length > 255) {
    $logger.warn(
      `Skipping rename (the new filename is greater than 255 characters): "${newFileName}"`
    );
    return {};
  }
  const parsed = $path.parse(scenePath);
  let newScenePath = $path.format({ dir: parsed.dir, name: newFileName, ext: parsed.ext });
  if (newScenePath === scenePath) {
    $logger.verbose(
      `Skipping rename (the filename already match the desired template): "${scenePath}"`
    );
    return {};
  }
  if (args.dry) {
    $logger.info(`Dry mode. Would have renamed "${scenePath}" to "${newScenePath}"`);
    return {};
  }
  if ($fs.existsSync(newScenePath)) {
    if (args.nameConflictHandling === "skip" /* SKIP */) {
      return {};
    }
    let counter = 1;
    while (args.nameConflictHandling === "rename" /* RENAME */ && $fs.existsSync(newScenePath)) {
      newScenePath = $path.format({
        dir: parsed.dir,
        name: `${newFileName}(${counter++})`,
        ext: parsed.ext
      });
    }
  }
  try {
    await $fsPromises.rename(scenePath, newScenePath);
  } catch (err) {
    $logger.error(`Could not rename "${scenePath}" to "${newScenePath}": ${$formatMessage(err)}`);
    return {};
  }
  $logger.info(`Renamed "${scenePath}" to "${newScenePath}"`);
  return { path: newScenePath };
};
handler.requiredVersion = "0.30.0-rc.3 - 1";
applyMetadata(handler, info_default);
var main_default = handler;
export {
  main_default as default
};
