import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

const runPlugin = createPluginRunner("analvids", plugin);

describe("Analvids", () => {
  describe("EKS vs KS", () => {
    it("Should extract EKS, not KS", async () => {
      const sceneName =
        "First Gangbang Nansy Small vs 4 big cocks ATM DP Deepthroat Rough & fast sex Cum in mouth Swallow EKS089";
      const result = await runPlugin({
        sceneName,
        args: {
          useSceneId: true,
          deep: false,
        },
      });
      expect(result.name).to.equal("EKS089");
    });
  });
});
