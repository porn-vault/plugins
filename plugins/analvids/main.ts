import { applyMetadata, Plugin, Context } from "../../types/plugin";

import $cheerio from "cheerio";
import $dayjs from "dayjs";

import utc from "dayjs/plugin/utc";
$dayjs.extend(utc);

import info from "./info.json";

type MyContext = Context & { sceneName?: string };

const SCENE_ID_REGEX = /\b[A-Z]{1,4}\d+/;

function extractShootId(originalTitle: string): string | null {
  const sceneIdMatch = originalTitle.match(SCENE_ID_REGEX); // detect studio prefixes
  const shootId = sceneIdMatch?.[0].trim() || null;
  return shootId;
}

async function directSearch(ctx: MyContext, sceneId: string): Promise<string | null> {
  ctx.$logger.verbose(`Getting scene: ${sceneId}`);

  const res = await fetch(`https://www.analvids.com/search?query=${sceneId}`);

  const url = res.url;
  if (url?.includes("/watch")) {
    return url;
  }

  return null;
}

async function autocompleteSearch(ctx: MyContext, sceneId: string): Promise<string | null> {
  ctx.$logger.verbose(`Searching for scenes using query: ${sceneId}`);

  const { terms } = await fetch(`https://www.analvids.com/api/autocomplete/search?q=${sceneId}`)
    .then(res => res.json()) as {
      terms: { type: "model" | "scene"; url: string; name: string }[];
    };

  const term = terms.find(({ name }) => name.toLowerCase().includes(sceneId.toLowerCase()));
  return term?.url || null;
}

async function getSceneUrl(ctx: MyContext, sceneId: string): Promise<string | null> {
  const directUrl = await directSearch(ctx, sceneId);
  if (directUrl) {
    return directUrl;
  }

  const searchUrl = await autocompleteSearch(ctx, sceneId);
  return searchUrl;
}

const handler: Plugin<MyContext, any> = async (ctx) => {
  const { $logger, sceneName, event } = ctx;

  const args = ctx.args as Partial<{
    dry: boolean;
    useSceneId: boolean;
    deep: boolean;
  }>;

  if (!sceneName) {
    $logger.warn(`Invalid event: ${event}`);
    return {};
  }

  $logger.verbose(`Extracting shoot ID from scene name: ${sceneName}`);

  const shootId = extractShootId(sceneName);
  if (shootId) {
    const cleanShootId = shootId.trim();
    $logger.info("Extracted scene ID: " + cleanShootId);

    const result: Record<string, unknown> = {
      custom: {
        "Shoot ID": cleanShootId,
        "Scene ID": cleanShootId,
      },
    };

    if (args.useSceneId) {
      result.name = cleanShootId;
      $logger.verbose("Setting name to shoot ID");
    }

    if (args.deep === false) {
      $logger.verbose("Not getting deep info");
    } else {
      const sceneUrl = await getSceneUrl(ctx, cleanShootId);

      if (!sceneUrl) {
        $logger.warn(`Scene not found for shoot ID: ${cleanShootId}`);
      } else {
        $logger.verbose(`Getting more scene info (deep: true): ${sceneUrl}`);

        // Scraping courtesy of traxxx (https://traxxx.me)
        const html = await fetch(sceneUrl).then(res => res.text());

        const $ = $cheerio.load(html, { normalizeWhitespace: true });

        if (!args.useSceneId) {
          const featuring = $(".watch__title .watch__featuring_models").text();
          const title = $("h1.watch__title").text().replace(featuring, "").trim();
          result.name = title;
        }

        result.releaseDate = $dayjs
          .utc($(".container-fluid .bi-calendar3").text(), "YYYY-MM-DD")
          .valueOf();

        result.description =
          $(".text-mob-more")
            .text()
            .replace(/\.{3}$/, "")
            .trim() || undefined;

        result.actors = $("h1.watch__title a")
          .map((_, el) => $(el).text())
          .toArray()
          .sort();

        result.labels = $(`.container-fluid .genres-list a[href*="/genre"]`)
          .map((_, el) => $(el).text())
          .toArray()
          .sort();

        result.studio = $(`.container-fluid .genres-list a[href*="/studios"]`).text().trim();
      }
    }

    if (args.dry) {
      $logger.warn(`Would have returned ${ctx.$formatMessage(result)}`);
      return {};
    }
    return result;
  }

  $logger.info("No scene ID found");

  return {};
};

handler.requiredVersion = "0.30.0-rc.3 - 1";

applyMetadata(handler, info);

export default handler;
