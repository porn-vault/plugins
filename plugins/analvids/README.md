## analvids 1.0.2

by boi123212321

Scrape Analvids (ex Legalporno) scene data

### Download links
Each download link is for the latest version of the plugin, for the indicated porn-vault server version.  
Make sure you are reading the documentation of the plugin, for the correct porn-vault server version.  
| Server version                                                                                                 | Plugin documentation                                                                            |
| -------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [Download link for: stable](https://gitlab.com/porn-vault/plugins/-/raw/master/dist/analvids.mjs?inline=false) | [documentation](https://gitlab.com/porn-vault/plugins/-/blob/master/plugins/analvids/README.md) |


### Arguments

| Name       | Type    | Required | Description                                 |
| ---------- | ------- | -------- | ------------------------------------------- |
| deep       | Boolean | false    | Fetch scene details                         |
| dry        | Boolean | false    | Whether to commit data changes              |
| useSceneId | Boolean | false    | Whether to set scene name to found shoot ID |

### Example installation with default arguments

`config.json`

```json
---
{
  "plugins": {
    "register": {
      "analvids": {
        "path": "./plugins/analvids.mjs",
        "args": {
          "deep": true,
          "dry": false,
          "useSceneId": false
        }
      }
    },
    "events": {
      "sceneCreated": [
        "analvids"
      ],
      "sceneCustom": [
        "analvids"
      ]
    }
  }
}
---
```

`config.yaml`

```yaml
---
plugins:
  register:
    analvids:
      path: ./plugins/analvids.mjs
      args:
        deep: true
        dry: false
        useSceneId: false
  events:
    sceneCreated:
      - analvids
    sceneCustom:
      - analvids

---

```
