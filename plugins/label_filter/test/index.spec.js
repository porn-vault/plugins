import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

import tests from "./index.fixture";

const runPlugin = createPluginRunner("label_filter", plugin);

describe("label_filter", () => {
  for (const test of tests) {
    it("Should work correctly", async () => {
      const result = await runPlugin({
        args: test.args,
        data: test.data,
      });
      expect(result).to.deep.equal(test.result);
    });
  }
});
