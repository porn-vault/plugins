import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

const runPlugin = createPluginRunner("adultEmpire", plugin);

describe("adultempire", () => {
  describe("Movies", () => {
    // I can't find a movie without back cover anymore
    it.skip("Should ignore missing back cover", async () => {
      const result = await runPlugin({
        movieName: "https://www.adultempire.com/2950080/rub-the-muff-8-porn-videos.html",
        args: {},
      });
      expect(result).to.be.an("object");
      expect(result.name).to.equal("Rub the Muff #8");
      expect(result.frontCover).to.be.a("string");
      expect(result.backCover).to.be.undefined;
    });
  });
});
