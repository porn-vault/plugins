## Plugin Details

This plugin retrieves data from adultempire.

NOTE: Unfortunately, sometimes movie front covers are uploaded as back covers, which can't be easily detected, so sometimes there may be false positives when downloading back covers.

