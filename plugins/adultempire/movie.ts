import $cheerio from "cheerio";
import $dayjs from "dayjs";

import { MovieContext, MovieOutput } from "../../types/movie";

interface MyContext extends MovieContext {
  args: {
    dry?: boolean;
  };
}

const REQ_OPTIONS = {
  headers: {
    Cookie: "AgeConfirmed=true"
  },
};

export function levenshtein(a: string, b: string): number {
  const an = a ? a.length : 0;
  const bn = b ? b.length : 0;
  if (an === 0) {
    return bn;
  }
  if (bn === 0) {
    return an;
  }
  const matrix = new Array<number[]>(bn + 1);
  for (let i = 0; i <= bn; ++i) {
    let row = matrix[i] = new Array<number>(an + 1);
    row[0] = i;
  }
  const firstRow = matrix[0];
  for (let j = 1; j <= an; ++j) {
    firstRow[j] = j;
  }
  for (let i = 1; i <= bn; ++i) {
    for (let j = 1; j <= an; ++j) {
      if (b.charAt(i - 1) === a.charAt(j - 1)) {
        matrix[i][j] = matrix[i - 1][j - 1];
      }
      else {
        matrix[i][j] = Math.min(
          matrix[i - 1][j - 1], // substitution
          matrix[i][j - 1], // insertion
          matrix[i - 1][j] // deletion
        ) + 1;
      }
    }
  }
  return matrix[bn][an];
}

async function searchForMovie(name: string): Promise<string | false> {
  const url = `https://www.adultempire.com/allsearch/search?q=${name}`;

  const html = await fetch(url, REQ_OPTIONS).then(res => res.text());

  const $ = $cheerio.load(html);

  const items = $(".product-card-wrapper a.boxcover").toArray().map(el => {
    const title = $(el).find("img").attr("title") ?? "";
    return {
      title,
      href: $(el).attr("href"),
      score: levenshtein(title, name),
    }
  }).filter(x => x.title && x.href);

  items.sort((a, b) => a.score - b.score);

  const bestResult = items.at(0);
  const href = bestResult?.href;

  if (!href) {
    return false;
  }
  return `https://www.adultempire.com${href.trim()}`;
}

async function urlAvailable(url: string) {
  const res = await fetch(url, {
    method: "HEAD",
    ...REQ_OPTIONS,
  });
  return res.ok;
}

export default async function (ctx: MyContext): Promise<MovieOutput> {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { args, $logger, $formatMessage, movieName, $createImage } = ctx;

  const name = movieName
    .replace(/[#&]/g, "")
    .replace(/\s{2,}/g, " ")
    .trim();
  $logger.info(`Scraping movie covers for '${name}', dry mode: ${args?.dry || false}...`);

  const url = movieName.startsWith("http") ? movieName : await searchForMovie(name);

  if (url) {
    const movieUrl = url;
    const html = await fetch(movieUrl, REQ_OPTIONS).then(res => res.text());
    const $ = $cheerio.load(html);

    const desc = $(".synopsis-content p")
      .get().map(p => $(p).text())
      .join("\n")
      .trim() || undefined;

    let release: number | undefined;

    let movieName = $(`.movie-page__heading__title`)
      .text()
      .replace(/[\t\n]+/g, " ")
      .replace(/ {2,}/, " ")
      .replace(/- On Sale!.*/i, "")
      .trim();

    if (movieName.endsWith(", The")) {
      movieName = movieName.replace(", The", "");
      movieName = `The ${movieName}`;
    } else if (movieName.endsWith(", A")) {
      movieName = movieName.replace(", A", "");
      movieName = `A ${movieName}`;
    }

    $(".col-sm-4.m-b-2 li").each((_, elm) => {
      const grabrvars = $(elm).text().split(":");
      if (grabrvars[0].includes("Released")) {
        release = $dayjs(grabrvars[1].trim().replace(" ", "-"), "MMM-DD-YYYY").valueOf();
      }
    });

    const studioName = $(`.title-rating-section .item-info > a`).eq(0).text().trim();

    const frontCover = $("#front-cover img").toArray()[0];
    const frontCoverSrc = $(frontCover).attr("src") || "";
    $logger.debug(`Got front cover URL: ${frontCoverSrc}`);

    let backCoverSrc: string | null = null;
    const backCover = $("#back-cover").toArray()[0];
    if (backCover) {
      backCoverSrc = $(backCover).attr("href") || "";
      $logger.debug(`Got back cover URL: ${backCoverSrc}`);
    }

    if (args?.dry === true) {
      $logger.info(
        `Would have returned ${$formatMessage({
          name: movieName,
          movieUrl,
          frontCoverSrc,
          backCoverSrc,
          studioName,
          desc,
          release,
        })}`
      );
    } else {
      const frontCoverImg = await $createImage(frontCoverSrc, `${movieName} (front cover)`);

      let backCoverImg: string | undefined;
      if (backCoverSrc) {
        backCoverImg = await $createImage(backCoverSrc, `${movieName} (back cover)`);
      }

      return {
        name: movieName,
        frontCover: frontCoverImg,
        backCover: backCoverImg,
        description: desc,
        releaseDate: release,
        studio: studioName,
      };
    }
  }

  return {};
}
