import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

const runPlugin = createPluginRunner("private", plugin);

describe("Private", () => {
  it("Should get scene by name", async () => {
    const result = await runPlugin({
      sceneName: "Kate Quinn's Threesome Fantasy Comes to Life",
      args: {
        useThumbnail: true,
      },
    });
    expect(result.name).to.equal("Kate Quinn’s Threesome Fantasy Comes to Life");
    expect(result.description).to.equal(
      "The spectacular young beauty, Kate Quinn, and her man, Raúl Costa, are our next couple up in Cuckhold Fantasies, and upon Raúl confessing that he wants to share and watch Kate fuck another man, their sex therapist has only one solution… a threesome! What better way to spice up your sex life than adding a friend into the mix, and that’s exactly what Kate and her cuckhold husband do today on www.private.com as they invite Lorenzo Viota to join them for a wild threesome that has Kate screaming with pleasure until her pretty face and beautiful ass are decorated with cum!"
    );
    expect(result.actors).to.deep.equal(["Kate Quinn", "Lorenzo Viota", "Raul Costa"]);
    expect(result.labels).to.deep.equal([
      "4K",
      "Brown Eyes",
      "Brunettes",
      "Cowgirl",
      "Cuckold",
      "Cum on Ass",
      "Cumshot",
      "Doggystyle",
      "FMM",
      "Facials",
      "HD",
      "Lingerie",
      "Masturbation",
      "Missionary",
      "Only Vaginal",
      "Pierced Belly",
      "Piercings",
      "Pussy licking",
      "Russian",
      "Shaved Pussy",
      "Small Tits",
      "Spoon Position",
      "Tattoo",
      "Teen (18+)",
      "Vaginal Sex",
      "Voyeur",
      "Young Woman",
    ]);
    expect(result.studio).to.equal("Tight and Teen");
    expect(result.thumbnail).to.be.a("string");
    expect(result.$thumbnailUrl).to.be.a("string");
  });

  // TODO: Hot Model Seduces Her Photographer finds miranda miller, not sirena...

  it("Should get scene by name 2", async () => {
    const result = await runPlugin({
      sceneName: "Sirena Hot Model Seduces Her Photographer",
      args: {
        useThumbnail: true,
      },
    });
    expect(result.name).to.equal("Sirena Milano, Hot Model Seduces Her Photographer");
    expect(result.description).to.equal(
      "Sirena Milano is a sexy redhead who has been recent sensation on Private, and today she returns in Hot Girls in Hot Cars for one of her best scenes yet! So don’t miss a single minute of Sirena in action as shows off her beautiful body on a sports car whilst her photographer Christian Clay gets a front seat to the show. Then watch the rest of the action unfold on www.private.com where they take things home for a wild afternoon of fun that includes a deepthroat blowjob for starters followed by a long and hard pounding that leaves Sirena’s beautiful face covered in cum!"
    );
    expect(result.actors).to.deep.equal(["Christian Clay", "Sirena Milano"]);
    expect(result.labels).to.deep.equal([
      "4K",
      "Blowjob",
      "Blue Eyes",
      "Cowgirl",
      "Deep throat",
      "Doggystyle",
      "Facials",
      "HD",
      "Hand Jobs",
      "Missionary",
      "Only Vaginal",
      "Pussy licking",
      "Redheads",
      "Reverse Cowgirl",
      "Rimming",
      "Rimming (man to girl)",
      "Russian",
      "Shaved Pussy",
      "Small Tits",
      "Spoon Position",
      "Teen (18+)",
      "Vaginal Sex",
      "Young Woman",
    ]);
    expect(result.studio).to.equal("Tight and Teen");
    expect(result.thumbnail).to.be.a("string");
    expect(result.$thumbnailUrl).to.be.a("string");
  });

  it("Should not attach thumbnail", async () => {
    const result = await runPlugin({
      sceneName: "Kate Quinn's Threesome Fantasy Comes to Life",
    });
    expect(result.name).to.equal("Kate Quinn’s Threesome Fantasy Comes to Life");
    expect(result.description).to.equal(
      "The spectacular young beauty, Kate Quinn, and her man, Raúl Costa, are our next couple up in Cuckhold Fantasies, and upon Raúl confessing that he wants to share and watch Kate fuck another man, their sex therapist has only one solution… a threesome! What better way to spice up your sex life than adding a friend into the mix, and that’s exactly what Kate and her cuckhold husband do today on www.private.com as they invite Lorenzo Viota to join them for a wild threesome that has Kate screaming with pleasure until her pretty face and beautiful ass are decorated with cum!"
    );
    expect(result.actors).to.deep.equal(["Kate Quinn", "Lorenzo Viota", "Raul Costa"]);
    expect(result.labels).to.deep.equal([
      "4K",
      "Brown Eyes",
      "Brunettes",
      "Cowgirl",
      "Cuckold",
      "Cum on Ass",
      "Cumshot",
      "Doggystyle",
      "FMM",
      "Facials",
      "HD",
      "Lingerie",
      "Masturbation",
      "Missionary",
      "Only Vaginal",
      "Pierced Belly",
      "Piercings",
      "Pussy licking",
      "Russian",
      "Shaved Pussy",
      "Small Tits",
      "Spoon Position",
      "Tattoo",
      "Teen (18+)",
      "Vaginal Sex",
      "Voyeur",
      "Young Woman",
    ]);
    expect(result.studio).to.equal("Tight and Teen");
    expect(result.thumbnail).to.be.undefined;
  });

  it("Should get scene by path", async () => {
    const result = await runPlugin({
      sceneName:
        "/some/imaginary/file/path/to/video/[Private] Kate Quinn - Kate Quinn's Threesome Fantasy Comes to Life.mp4",
      args: {
        useThumbnail: true,
      },
    });
    expect(result.name).to.equal("Kate Quinn’s Threesome Fantasy Comes to Life");
    expect(result.description).to.equal(
      "The spectacular young beauty, Kate Quinn, and her man, Raúl Costa, are our next couple up in Cuckhold Fantasies, and upon Raúl confessing that he wants to share and watch Kate fuck another man, their sex therapist has only one solution… a threesome! What better way to spice up your sex life than adding a friend into the mix, and that’s exactly what Kate and her cuckhold husband do today on www.private.com as they invite Lorenzo Viota to join them for a wild threesome that has Kate screaming with pleasure until her pretty face and beautiful ass are decorated with cum!"
    );
    expect(result.actors).to.deep.equal(["Kate Quinn", "Lorenzo Viota", "Raul Costa"]);
    expect(result.labels).to.deep.equal([
      "4K",
      "Brown Eyes",
      "Brunettes",
      "Cowgirl",
      "Cuckold",
      "Cum on Ass",
      "Cumshot",
      "Doggystyle",
      "FMM",
      "Facials",
      "HD",
      "Lingerie",
      "Masturbation",
      "Missionary",
      "Only Vaginal",
      "Pierced Belly",
      "Piercings",
      "Pussy licking",
      "Russian",
      "Shaved Pussy",
      "Small Tits",
      "Spoon Position",
      "Tattoo",
      "Teen (18+)",
      "Vaginal Sex",
      "Voyeur",
      "Young Woman",
    ]);
    expect(result.studio).to.equal("Tight and Teen");
    expect(result.thumbnail).to.be.a("string");
    expect(result.$thumbnailUrl).to.be.a("string");
  });

  for (const [sceneName, expectedName] of [
    [
      "Anal Introductions - Dolly Dyson - Dolly Dyson enjoys an Anal Fuck with squirting and a Creampie - anal squirt creampie - 2024-11-12",
      "Dolly Dyson Enjoys an Anal Fuck with Squirting and a Creampie",
    ],
    [
      "Anal Introductions - Dolly Dyson - Blowjob at the Beach and Anal Ride - anal - 2023-09-09",
      "Dolly Dyson, Blowjob at the Beach and Anal Ride",
    ],
    [
      "Candee Licious, Sneaky sex with a beach vendor, full anal later by the pool",
      "Candee Licious, Sneaky sex with a beach vendor, full anal later by the pool",
    ],
    [
      "Tight and Teen - Marie Berger - Marie Berger Debuts with her first Interracial - 2022-01-06",
      "Marie Berger Debuts With Her First Interracial",
    ],
    [
      "/media/disk2/DATA/Ariana Van X - Wet Memories - Tight and Teen [full nude, shaved, creampie].mp4",
      "Ariana Van X, Wet Memories",
    ],
    [
      "/media/disk2/DATA/Ariana Van X - Sun and Sex - Tight and Teen [shaved, lingerie, stockings, facial].mp4",
      "Ariana Van X, Sun and Sex",
    ],
    ["Ariana Van X - Sun and Sex", "Ariana Van X, Sun and Sex"],
    ["Sun and Sex", "Ariana Van X, Sun and Sex"],
    ["Wet Memories", "Ariana Van X, Wet Memories"],
  ]) {
    it("Should pass matching case", async () => {
      const result = await runPlugin({
        sceneName,
      });
      expect(result.name).to.equal(expectedName);
    });
  }
});
