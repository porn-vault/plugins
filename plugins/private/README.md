## private 0.2.0

by boi123212321

Scrape data from Private

### Download links
Each download link is for the latest version of the plugin, for the indicated porn-vault server version.  
Make sure you are reading the documentation of the plugin, for the correct porn-vault server version.  
| Server version                                                                                                | Plugin documentation                                                                           |
| ------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| [Download link for: stable](https://gitlab.com/porn-vault/plugins/-/raw/master/dist/private.mjs?inline=false) | [documentation](https://gitlab.com/porn-vault/plugins/-/blob/master/plugins/private/README.md) |


### Arguments

| Name         | Type    | Required | Description                       |
| ------------ | ------- | -------- | --------------------------------- |
| useThumbnail | Boolean | false    | Download & attach scene thumbnail |

### Example installation with default arguments

`config.json`

```json
---
{
  "plugins": {
    "register": {
      "private": {
        "path": "./plugins/private.mjs",
        "args": {
          "useThumbnail": false
        }
      }
    },
    "events": {}
  }
}
---
```

`config.yaml`

```yaml
---
plugins:
  register:
    private:
      path: ./plugins/private.mjs
      args:
        useThumbnail: false
  events: {}

---

```
