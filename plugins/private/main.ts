import cheerio from "cheerio";

import { applyMetadata, type Plugin } from "../../types/plugin";
import type { SceneContext, SceneOutput } from "../../types/scene";

import info from "./info.json";

const windows = <T>(l: number, xs: T[]) =>
  xs.flatMap((_, i) => (i <= xs.length - l ? [xs.slice(i, i + l)] : []));

async function getSearchPageDom(q: string, page = 1) {
  const searchUrl = `https://www.private.com/search.php?page=${page}&query=${encodeURIComponent(
    q
  )}`;

  const res = await fetch(searchUrl, {
    headers: {
      Cookie: "agreed18=true; customLang=en",
    },
  });
  const html = await res.text();

  return cheerio.load(html);
}

const handler: Plugin<SceneContext, SceneOutput> = async (ctx) => {
  const { $createImage, $logger, sceneName, scenePath, event, args } = ctx;

  const q = sceneName || scenePath;

  if (!q) {
    $logger.warn(`Invalid event: ${event}`);
    return {};
  }

  const search$ = await getSearchPageDom(q);
  const search2$ = await getSearchPageDom(q, 2);
  const cards = search$("#search_results .card").toArray();
  const cards2 = search2$("#search_results .card").toArray();

  let data = [...cards, ...cards2].map((card) => {
    return {
      name: search$(card).find("h3").text().trim(),
      url: search$(card).find("a").attr("href"),
      score: 0,
    };
  });

  const normalizedQuery = q.toLowerCase().trim();

  const rawTokens = normalizedQuery.split(" ");
  const grams = new Set([
    ...rawTokens,
    ...windows(2, rawTokens).map((s) => s.join(" ")),
    ...windows(3, rawTokens).map((s) => s.join(" ")),
    ...windows(4, rawTokens).map((s) => s.join(" ")),
    ...windows(5, rawTokens).map((s) => s.join(" ")),
  ]);

  data = data.map((card) => {
    const rawTokens = card.name.toLowerCase().split(" ");
    let score = 0;

    for (let len = 1; len <= 5; len++) {
      for (const token of windows(len, rawTokens)) {
        if (grams.has(token.join(" "))) {
          score += len;
        }
      }
    }

    return {
      ...card,
      score,
    };
  });

  const bestFit = data.sort((a, b) => b.score - a.score).at(0);

  if (!bestFit?.url) {
    $logger.warn(`No result found for ${q}`);
    return {};
  }

  const res = await fetch(bestFit.url, {
    headers: {
      Cookie: "agreed18=true; customLang=en",
    },
  });
  const html = await res.text();
  const $ = cheerio.load(html);

  const title = $(".title-zone h1").text().trim();

  const actors = $(".tag-models")
    .toArray()
    .map((x) => $(x).text().trim())
    .sort();

  const labels = $(".tag-tags")
    .toArray()
    .map((x) => $(x).text().trim())
    .sort();

  const studio = $(".tag-sites").text().trim();

  const thumbnailUrl = $(`meta[property="og:image"]`).attr("content");

  let thumbnail;

  if ((args as any).useThumbnail === true && thumbnailUrl) {
    thumbnail = await $createImage(thumbnailUrl, title, true);
  }

  return {
    name: title,
    description: $("#description-section").text().trim(),
    actors,
    labels,
    thumbnail: (args as any).useThumbnail === true ? thumbnail : undefined,
    studio,

    $thumbnailUrl: thumbnailUrl,
  };
};

handler.requiredVersion = "0.30.0-rc.3 - 1"; // TODO: adjust version requirement here

applyMetadata(handler, info);

export default handler;
