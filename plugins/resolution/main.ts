import { applyMetadata, type Plugin } from "../../types/plugin";
import type { SceneContext, SceneOutput } from "../../types/scene";

import info from "./info.json";

interface MyContext {
  args: {
    resolutions?: unknown;
  };
}

const handler: Plugin<SceneContext & MyContext, SceneOutput> = async (ctx) => {
  const { args, $throw, data, scenePath, scene } = ctx;

  if (!scenePath) {
    throw $throw("Uh oh. You shouldn't use the plugin for this type of event");
  }

  let resolutions = [144, 240, 360, 480, 720, 1080, 2160];
  if (args.resolutions) {
    if (!Array.isArray(args.resolutions) || args.resolutions.some((x) => typeof x !== "number")) {
      throw $throw("Invalid resolutions array");
    }
    resolutions = args.resolutions.map(Math.floor);
  }

  if (scene.meta.dimensions && scene.meta.dimensions.height) {
    const formattedResolution = `${scene.meta.dimensions.height}p`;
    if (data.labels) {
      return {
        labels: [...data.labels, formattedResolution],
      };
    }
    return {
      labels: [formattedResolution],
    };
  }

  const resolution = resolutions.find((res) => scenePath.toLowerCase().includes(`${res}p`));

  if (resolution) {
    const formattedResolution = `${resolution}p`;
    if (data.labels) {
      return {
        labels: [...data.labels, formattedResolution],
      };
    }
    return {
      labels: [formattedResolution],
    };
  }

  return {};
};

handler.requiredVersion = "0.30.0-rc.3 - 1";

applyMetadata(handler, info);

export default handler;
