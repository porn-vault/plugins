Currently custom fields can only be named as follows (not case sensitive):

- Hair Color
- Eye Color
- Ethnicity
- Birthplace
- Zodiac
- Started
- Ended
