import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

const runPlugin = createPluginRunner("freeones", plugin);

function search(args = {}) {
  return runPlugin({
    actorName: "Haley Reed",
    args,
  });
}

describe("freeones", () => {
  it("Search 'Haley Reed'", async () => {
    const result = await search();
    expect(result.custom).to.deep.equal({
      "hair color": "Blonde",
      "eye color": "Green",
      ethnicity: "Caucasian",
      /*  height: 177,
      weight: 53,
      "waist size": 60,
      "hip size": 88,
      "cup size": "B", */
      birthplace: "Orlando, FL",
      /*  "bra size": "34B",
      "bust size": 86, */
      started: 2016,
      gender: "Female",
      sex: "Female",
      piercings: "Nipple rings; septum",
      tattoos: "Yes",
    });
    // expect(result.nationality).to.equal("US");
    expect(result.bornOn).to.be.a("number");
    expect(result.labels).to.have.length.greaterThan(0);
    expect(result.labels).to.contain("Blonde Hair");
    expect(result.labels).to.contain("Green Eyes");
    expect(result.labels).to.contain("Caucasian");
    expect(result.labels).to.contain("Female");
    expect(result.labels).to.contain("Piercings");
    expect(result.labels).to.contain("Tattoos");
  });
});
