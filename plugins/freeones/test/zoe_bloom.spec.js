import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

const runPlugin = createPluginRunner("freeones", plugin);

function search(args = {}) {
  return runPlugin({
    actorName: "Zoe Bloom",
    args,
  });
}

describe("freeones", () => {
  it("Search 'Zoe Bloom'", async () => {
    const result = await search({
      dry: false,
      blacklist: [],
      useImperial: false,
      useAvatarAsThumbnail: false,
    });
    expect(result.custom).to.deep.equal({
      "hair color": "Brown",
      "eye color": "Green",
      ethnicity: "Caucasian",
      /*       height: 154,
      weight: 49, */
      birthplace: "Pittsburgh, PA",
      /*    "waist size": 60,
      "hip size": 88,
      "cup size": "A",
      "bra size": "32A",
      "bust size": 81, */
      started: 2018,

      gender: "Female",
      sex: "Female",
      piercings: "Left nostril",
      tattoos: "Yes",
    });
    // expect(result.nationality).to.equal("US");
    expect(result.bornOn).to.be.a("number");
    expect(result.labels).to.have.length.greaterThan(0);
    expect(result.labels).to.contain("Brown Hair");
    expect(result.labels).to.contain("Green Eyes");
    expect(result.labels).to.contain("Caucasian");
    expect(result.labels).to.contain("Female");
    expect(result.labels).to.contain("Piercings");
    expect(result.labels).to.contain("Tattoos");
  });

  it("Search 'Zoe Bloom' but without measurements", async () => {
    const result = await search({
      dry: false,
      blacklist: ["measurements"],
      useImperial: false,
      useAvatarAsThumbnail: false,
    });
    expect(result.custom).to.deep.equal({
      "hair color": "Brown",
      "eye color": "Green",
      ethnicity: "Caucasian",
      /* height: 154,
      weight: 49, */
      birthplace: "Pittsburgh, PA",
      started: 2018,
      gender: "Female",
      sex: "Female",
      piercings: "Left nostril",
      tattoos: "Yes",
    });
    //expect(result.nationality).to.equal("US");
    expect(result.bornOn).to.be.a("number");
    expect(result.labels).to.have.length.greaterThan(0);
    expect(result.labels).to.contain("Brown Hair");
    expect(result.labels).to.contain("Green Eyes");
    expect(result.labels).to.contain("Caucasian");
    expect(result.labels).to.contain("Female");
    expect(result.labels).to.contain("Piercings");
    expect(result.labels).to.contain("Tattoos");
  });
});
