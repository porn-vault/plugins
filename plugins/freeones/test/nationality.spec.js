import { describe, it, expect } from "vitest";

import { createPluginRunner } from "../../../context";
import plugin from "../main";

const runPlugin = createPluginRunner("freeones", plugin);

/**
 * @type [string, string][]
 */
const fixtures = [
  ["Ginebra Bellucci", "ES"],
  ["Kinuski", "FI"],
  ["Emelie Crystal", "DE"],
  ["Angelika Grays", "UA"],
  ["Kira Thorn", "RU"],
  ["Jill Kassidy", "US"],
  ["Ella Hughes", "GB"],
  ["Lina Luxa", "FR"],
  ["Moona Snake", "IT"],
  ["Stacy Cruz", "CZ"],
  ["Valentina Milan", "CO"],
];

describe("freeones", () => {
  describe("nationality", () => {
    for (const [actorName, alpha2] of fixtures) {
      it(`${actorName} is ${alpha2}`, async () => {
        const result = await runPlugin({
          actorName,
          args: {},
        });
        expect(result.nationality).to.equal(alpha2);
      });
    }
  });
});
