import { applyMetadata, Plugin } from "../../types/plugin";
import { ActorContext, ActorOutput } from "../../types/actor";

import $dayjs from "dayjs";
import * as $cheerio from "cheerio";

import info from "./info.json";

interface MyContext extends ActorContext {
  args: {
    whitelist?: string[];
    blacklist?: string[];
    dry?: boolean;
    useImperial?: boolean;
    searchResultsSort?: "string";
    useAvatarAsThumbnail?: boolean;
    piercingsType?: "string" | "array";
    tattoosType?: "string" | "array";
  };
}

function lowercase(str: string): string {
  return str.toLowerCase();
}

function cmToFt(cm: number): number {
  cm *= 0.033;
  return Math.round((cm + Number.EPSILON) * 100) / 100;
}

function kgToLbs(kg: number): number {
  kg *= 2.2;
  return Math.round((kg + Number.EPSILON) * 100) / 100;
}

async function search(query: string, sort: string): Promise<string> {
  const url = `https://www.freeones.com/partial/subject`;

  const html = await fetch(`${url}?q=${query}&s=${sort}`)
    .then(res => res.text());

  return html;
}

async function getFirstSearchResult(ctx: MyContext, query: string): Promise<cheerio.Cheerio> {
  const searchHtml = await search(query, ctx.args.searchResultsSort || "relevance");
  const $ = $cheerio.load(searchHtml);
  const el = $(".grid-item.teaser-subject>a");
  return el;
}

const handler: Plugin<MyContext, ActorOutput> = async (ctx) => {
  const { $createImage, args, $throw, $logger, $formatMessage, actorName } = ctx;
  if (!actorName) {
    $throw("Uh oh. You shouldn't use the plugin for this type of event");
  }

  $logger.info(`Scraping freeones for ${actorName}, dry mode: ${args.dry || false}...`);

  const blacklist = (args.blacklist || []).map(lowercase);
  if (!args.blacklist) {
    $logger.verbose("No blacklist defined, returning everything...");
  }
  if (blacklist.length) {
    $logger.verbose(`Blacklist defined, will ignore: ${blacklist.join(", ")}`);
  }

  const whitelist = (args.whitelist || []).map(lowercase);
  if (whitelist.length) {
    $logger.verbose(`Whitelist defined, will only return: ${whitelist.join(", ")}...`);
  }

  function isBlacklisted(prop: string): boolean {
    if (whitelist.length) {
      return !whitelist.includes(lowercase(prop));
    }
    return blacklist.includes(lowercase(prop));
  }

  const searchResultsSort = args.searchResultsSort;
  if (!searchResultsSort) {
    $logger.verbose("searchResultsSort preference not set. Using default 'relevance' value...");
  } else {
    $logger.verbose(`Search results will be ordered by key: ${searchResultsSort}.`);
  }

  // Check imperial unit preference
  const useImperial = args.useImperial;
  if (!useImperial) {
    $logger.verbose("Imperial preference not set. Using metric values...");
  } else {
    $logger.verbose("Imperial preference indicated. Using imperial values...");
  }

  // Check Use Avatar as Thumbnail preference
  const useAvatarAsThumbnail = args.useAvatarAsThumbnail;
  if (!useAvatarAsThumbnail) {
    $logger.verbose("Will not use the Avatar as the Actor Thumbnail...");
  } else {
    $logger.verbose("Will use the Avatar as the Actor Thumbnail...");
  }

  let firstResult: cheerio.Cheerio;
  try {
    $logger.debug(`Searching for ${actorName}`);
    firstResult = await getFirstSearchResult(ctx, actorName);
  } catch (error: any) {
    $throw(error?.message);
    return {}; // return for type compatibility
  }

  if (!firstResult) {
    $throw(`${actorName} not found!`);
  }

  const href = firstResult.attr("href")?.replace("/feed", "");

  if (!href) {
    $logger.info(`No result found`);
    return {};
  }

  let html: string;
  try {
    const url = `https://freeones.com${href.trim()}/bio`;
    $logger.debug(`GET ${url}`);
    html = await fetch(url).then(res => res.text());
  } catch (error: any) {
    $throw(error?.message);
    return {}; // return for type compatibility
  }
  const $ = $cheerio.load(html);

  function getNationality(): Partial<{ nationality: string }> {
    if (isBlacklisted("nationality")) {
      return {};
    }
    $logger.verbose("Getting nationality...");

    const selector = $(".header-content .flag-icon");

    if (!selector.length) {
      $logger.verbose("Nationality not found");
      return {};
    }

    const selectorClasses = selector.attr("class")!.split(" ");
    const flagClass = selectorClasses.find((x) => x.startsWith("flag-icon-"));
    const alpha2 = flagClass?.replace("flag-icon-", "").toUpperCase();

    if (!alpha2) {
      return {};
    }

    return {
      nationality: alpha2,
    };
  }

  function getHeight(): Partial<{ height: number }> {
    if (isBlacklisted("height")) {
      return {};
    }
    $logger.verbose("Getting height...");

    const selector = $('[data-test="link_span_height"]');
    if (!selector) {
      return {};
    }

    const rawHeight = $(selector).text();
    const rawHeightMatch = rawHeight.match(/\d+ ?cm/);
    const cm = rawHeightMatch?.[0] || null;
    if (!cm) {
      return {};
    }
    const height = parseInt(cm.replace("cm", "").trim());
    if (!useImperial) {
      return { height };
    }

    // Convert to imperial
    return { height: cmToFt(height) };
  }

  function getWeight(): Partial<{ weight: number }> {
    if (isBlacklisted("weight")) {
      return {};
    }
    $logger.verbose("Getting weight...");

    const selector = $('[data-test="link_span_weight"]');
    if (!selector) {
      return {};
    }

    const rawWeight = $(selector).text();
    const rawWeightMatch = rawWeight.match(/\d+ ?kg/);
    const kg = rawWeightMatch?.[0] || null;
    if (!kg) {
      return {};
    }
    const weight = parseInt(kg.replace("kg", "").trim());
    if (!useImperial) {
      return { weight };
    }

    // Convert to imperial
    return { weight: kgToLbs(weight) };
  }

  function computeZodiac(timestamp: number): string | undefined {
    const inputDate = $dayjs(timestamp);
    if (!inputDate.isValid()) {
      return;
    }

    const day = inputDate.date();
    const month = inputDate.month();
    // signSwitchDay[i] gives the boundary day between two signs, for the month at index 'i' (according to Britannica Encyclopedia)
    const signSwitchDay = [20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 22, 22];
    const signAtMonthStart = [
      "Capricorn",
      "Aquarius",
      "Pisces",
      "Aries",
      "Taurus",
      "Gemini",
      "Cancer",
      "Leo",
      "Virgo",
      "Libra",
      "Scorpio",
      "Sagittarius",
    ];

    // signSwitchDay[0] gives 20, meaning any day less than 20 at month index 0 (january) is the sign at the start of that month index (capricorn),
    // and any day greater than 20 is the sign at the start of the next month: i+1 (aquarius)
    const isBeforeSwithchDay = day <= signSwitchDay[month];

    // Mod 12 allows us to cycle between 0-11, so if the day is past the boundary day of month index 11 (december), we'll get index 0 (january)
    return signAtMonthStart[isBeforeSwithchDay ? month : (month + 1) % 12];
  }

  function getZodiac(): Partial<{ zodiac: string }> {
    if (isBlacklisted("zodiac")) {
      return {};
    }

    const bornOn = getAge().bornOn;
    if (!bornOn) {
      $logger.verbose("No birth date found => zodiac will be empty.");
      return {};
    }
    const computedZodiac = computeZodiac(bornOn.valueOf());
    $logger.verbose(
      `Computed zodiac sign for: ${new Date(bornOn).toLocaleDateString()}: ${computedZodiac}`
    );

    return { zodiac: computedZodiac };
  }

  function getBirthplace(): Partial<{ birthplace: string }> {
    if (isBlacklisted("birthplace")) {
      return {};
    }
    $logger.verbose("Getting birthplace...");

    const selector = $('.profile-meta-list a[href*="placeOfBirth"]');
    const cityName = selector.length ? $(selector).text() : null;

    if (!cityName) {
      $logger.verbose("No birthplace found");
      return {};
    } else {
      const stateSelector = $('.profile-meta-list a[href*="province"]');
      const stateName = stateSelector.length ? $(stateSelector).text() : null;
      if (!stateName) {
        $logger.verbose("No birth province found, just city!");
        return { birthplace: cityName.trim() };
      } else {
        const bplace = `${cityName}, ${stateName.split("-")[0].trim()}`.trim();
        return { birthplace: bplace };
      }
    }
  }

  function scrapeText<K extends string>(prop: K, selector: string): Partial<{ [P in K]: string }> {
    if (isBlacklisted(prop)) {
      return {};
    }
    $logger.verbose(`Scraping ${prop}`);

    const el = $(selector);
    if (!el) {
      return {};
    }

    return {
      [prop]: el.text().trim(),
    } as any;
  }

  async function getAvatar(): Promise<Partial<{ avatar: string; thumbnail: string }>> {
    if (args.dry) {
      return {};
    }
    if (isBlacklisted("avatar") && !useAvatarAsThumbnail) {
      // If not using either avatar or thumbnail, return nothing
      return {};
    }
    $logger.verbose("Getting avatar (and/or thumbnail)...");

    const imgEl = $(`.dashboard-header img.img-fluid`);
    if (!imgEl) {
      return {};
    }

    const url = $(imgEl).attr("src");

    if (!url) {
      return {};
    }

    const imgId = await $createImage(url, `${actorName} (avatar)`);
    const result: Partial<{ avatar: string; thumbnail: string }> = {};

    if (!isBlacklisted("avatar")) {
      result.avatar = imgId;
    }
    if (useAvatarAsThumbnail) {
      result.thumbnail = imgId;
    }

    return result;
  }

  function getAge(): Partial<{ bornOn: number }> {
    if (isBlacklisted("bornOn")) {
      return {};
    }
    $logger.verbose("Getting age...");

    const aTag = $(" [data-test='link_dateOfBirth']");
    if (!aTag) {
      return {};
    }

    const dateLiteral = aTag.text();
    const dateTokens = dateLiteral.match(/([A-z]*)\s(\d+),\s(\d+)/);

    if (dateTokens && dateTokens.length) {
      const timestamp = $dayjs(
        `${dateTokens[1]}-${dateTokens[2]}-{${dateTokens[3]}`,
        `MMMM-DD-YYYY`
      ).valueOf();

      return {
        bornOn: timestamp,
      };
    } else {
      $logger.verbose("Could not find actor birth date.");
      return {};
    }
  }

  function getAlias(): Partial<{ aliases: string[] }> {
    if (isBlacklisted("aliases")) {
      return {};
    }
    $logger.verbose("Getting aliases...");

    const aliasSel = $('[data-test="section-alias"] p[data-test*="p_aliases"]');
    const aliasText = aliasSel.text();
    const aliasName = aliasText && !/unknown/.test(aliasText) ? aliasText.trim() : null;
    if (!aliasName) {
      return {};
    }
    const aliases = aliasName.split(/,\s*/g);

    return { aliases };
  }

  function getCareer(): Partial<{
    started: number;
    ended: number;
  }> {
    if (isBlacklisted("career")) {
      return {};
    }
    $logger.verbose("Getting career information...");

    const careerSel = $(".timeline-horizontal p.m-0");
    if (!careerSel) {
      return {};
    }

    const career: Partial<{
      started: number;
      ended: number;
    }> = {};

    const careerStart = $(careerSel[0]).text();
    if (careerStart && careerStart !== "Begin") {
      career.started = Number.parseInt(careerStart, 10);
      if (Number.isNaN(career.started)) {
        delete career.started;
      }
    }

    const careerEnd = $(careerSel[1]).text();
    if (careerEnd && careerEnd !== "Now") {
      career.ended = Number.parseInt(careerEnd, 10);
      if (Number.isNaN(career.ended)) {
        delete career.ended;
      }
    }

    return career;
  }

  function getWaistSize(): Partial<{ "waist size": number }> {
    if (isBlacklisted("measurements")) {
      return {};
    }
    $logger.verbose("Getting waist size...");

    const rawWaist = scrapeText("waist size", `[data-test="link_span_waist"]`);

    const rawHipMatch = rawWaist["waist size"]?.match(/\d+ ?cm/);
    const cm = rawHipMatch?.[0] || null;
    if (!cm) {
      return {};
    }
    const hip = parseInt(cm.replace("cm", "").trim());
    if (!useImperial) {
      return { "waist size": hip };
    }

    // Convert to imperial
    return { "waist size": cmToFt(hip) };
  }

  function getHipSize(): Partial<{ "hip size": number }> {
    if (isBlacklisted("measurements")) {
      return {};
    }
    $logger.verbose("Getting hip size...");

    const rawHip = scrapeText("hip size", `[data-test="link_span_hip"]`);

    const rawHipMatch = rawHip["hip size"]?.match(/\d+ ?cm/);
    const cm = rawHipMatch?.[0] || null;
    if (!cm) {
      return {};
    }
    const hip = parseInt(cm.replace("cm", "").trim());
    if (!useImperial) {
      return { "hip size": hip };
    }

    // Convert to imperial
    return { "hip size": cmToFt(hip) };
  }

  function getCupSize(): Partial<{ "cup size": string }> {
    if (isBlacklisted("measurements")) {
      return {};
    }
    $logger.verbose("Getting cup size...");

    return scrapeText("cup size", `[data-test="link_span_cup"]`);
  }

  function getBustSize(): Partial<{ "bust size": number }> {
    if (isBlacklisted("measurements")) {
      return {};
    }
    $logger.verbose("Getting bust size...");

    const result = scrapeText("bust size", `[data-test="link_span_bust"]`);
    return result["bust size"] ? { "bust size": parseInt(result["bust size"]) } : {};
  }

  function getBraSize(): Partial<{ "bra size": string }> {
    if (isBlacklisted("measurements")) {
      return {};
    }
    $logger.verbose("Getting bra size...");

    return scrapeText("bra size", `[data-test="link_span_bra"]`);
  }

  function getGender(): Partial<{ sex: string; gender: string }> {
    if (isBlacklisted("gender")) {
      return {};
    }
    return { sex: "Female", gender: "Female" };
  }

  function getTattoos(): Partial<{ tattoos: string | string[] }> {
    if (isBlacklisted("tattoos")) {
      return {};
    }
    const tattooResult = scrapeText("tattoos", '[data-test="link_span_tattoos"]');
    if (tattooResult.tattoos === "Yes") {
      return {
        tattoos: "Yes",
      };
    }

    return {};
  }

  function getPiercings(): Partial<{ piercings: string | string[] }> {
    if (isBlacklisted("piercings")) {
      return {};
    }
    const res: { piercings?: string } = scrapeText(
      "piercings",
      '[data-test="link_span_piercingLocations"]'
    );
    const piercingText = res.piercings?.trim();
    if (!piercingText || /No Piercings/i.test(piercingText)) {
      return {};
    }

    if (args.piercingsType === "array") {
      return { piercings: piercingText.split(";").map((s) => s.trim()) };
    }

    return { piercings: piercingText };
  }

  const custom = {
    ...scrapeText("hair color", '[data-test="link_hair_color"] .text-underline-always'),
    ...scrapeText("eye color", '[data-test="link_eye_color"] .text-underline-always'),
    ...scrapeText("ethnicity", '[data-test="link_ethnicity"] .text-underline-always'),
    /* ...getHeight(),
    ...getWeight(),
    ...getCupSize(),
    ...getBustSize(),
    ...getWaistSize(),
    ...getHipSize(),
    ...getBraSize(), */
    ...getBirthplace(),
    ...getZodiac(),
    ...getGender(),
    ...getTattoos(),
    ...getCareer(),
    ...getPiercings(),
  };

  if (custom.tattoos === "Unknown") {
    delete custom.tattoos;
  }

  const data: ActorOutput = {
    ...getNationality(),
    ...getAge(),
    ...getAlias(),
    ...(await getAvatar()),
    custom,
  };

  if (!isBlacklisted("labels")) {
    data.labels = [];
    if (custom["hair color"]) {
      data.labels.push(`${custom["hair color"]} Hair`);
    }
    if (custom["eye color"]) {
      data.labels.push(`${custom["eye color"]} Eyes`);
    }
    if (custom.ethnicity) {
      data.labels.push(custom.ethnicity);
    }
    if (custom.gender) {
      data.labels.push("Female");
    }
    if (custom.piercings) {
      data.labels.push("Piercings");
    }
    if (custom.tattoos) {
      data.labels.push("Tattoos");
    }
  }

  if (args.dry === true) {
    $logger.info(`Would have returned: ${$formatMessage(data)}`);
    return {};
  }
  return data;
};

handler.requiredVersion = "0.30.0-rc.3 - 1";

applyMetadata(handler, info);

export default handler;
