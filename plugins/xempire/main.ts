import $dayjs from "dayjs";

import utc from "dayjs/plugin/utc";
$dayjs.extend(utc);

import { applyMetadata, Plugin, Context } from "../../types/plugin";
import { SceneOutput } from "../../types/scene";

import info from "./info.json";

type MyContext = Context & { scenePath?: string; sceneName?: string };

const refererUrl = "https://www.xempire.com/";
const userAgent =
  "Algolia for vanilla JavaScript (lite) 3.27.0;instantsearch.js 2.7.4;JS Helper 2.26.0";
const SITE_NAMES = ["hardx", "darkx", "lesbianx", "xempire", "allblackx", "eroticax"];

async function getCredentials() {
  const body = await fetch(refererUrl).then(res => res.text());

  const apiLine = body.split("\n").find((bodyLine: string) => bodyLine.match("apiKey"));
  if (!apiLine) {
    throw new Error(`Could not extract XEmpire API Key from ${refererUrl}`);
  }

  const apiSerial = apiLine.slice(apiLine.indexOf("{"), apiLine.indexOf("};") + 1);
  const apiData = JSON.parse(apiSerial) as {
    api: {
      algolia: {
        applicationID: string;
        apiKey: string;
      };
    };
  };

  const { applicationID: appId, apiKey } = apiData.api.algolia;

  const apiUrl = `https://${appId.toLowerCase()}-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=${userAgent}&x-algolia-application-id=${appId}&x-algolia-api-key=${apiKey}`;

  return { appId, apiKey, apiUrl };
}

function getImageUrl(path: string): string {
  return `https://images01-evilangel.gammacdn.com/movies${path}`;
}

const handler: Plugin<MyContext, SceneOutput> = async (ctx) => {
  const { $createImage, $logger, sceneName, scenePath, event } = ctx;

  const q = sceneName || scenePath;

  if (!q) {
    $logger.warn(`Invalid event: ${event}`);
    return {};
  }

  if (!SITE_NAMES.some((x) => q.toLowerCase().includes(x))) {
    $logger.verbose(`No XEmpire Studio found in: ${q}`);
    return {};
  }

  const { apiUrl } = await getCredentials();

  const algoliaRes = await fetch(apiUrl, {
    method: "POST",
    body: JSON.stringify({
      requests: [
        {
          indexName: "all_scenes",
          params: `query=${q.replace("&", "")}`,
        },
      ],
    }),
    headers: {
      Referer: refererUrl,
    }
  });

  const body = await algoliaRes.json() as {
    results?: {
      hits?: {
        title: string;
        // eslint-disable-next-line camelcase
        release_date: string; // YYYY-MM-DD
        actors: {
          name: string;
          gender: "male" | "female";
        }[];
        categories: {
          name: string;
        }[];
        // eslint-disable-next-line camelcase
        studio_name: string;
        description: string;
        pictures?: {
          nsfw: {
            top: {
              "1920x1080": string;
            };
          };
        };
      }[];
    }[];
  };

  const first = body.results?.[0]?.hits?.find((x) =>
    q.toLowerCase().includes(x.title.toLowerCase())
  );

  if (!first) {
    $logger.warn("No search result");
    return {};
  }

  let thumbnail: string | undefined;
  if (first.pictures?.nsfw?.top?.["1920x1080"]) {
    thumbnail = await $createImage(
      getImageUrl(first.pictures.nsfw.top["1920x1080"]),
      first.title,
      true
    );
  }

  // TODO: useThumbnail

  return {
    name: first.title.trim(),
    releaseDate: $dayjs.utc(first.release_date, "YYYY-MM-DD").toDate().valueOf(),
    description: first.description.trim(),
    actors: first.actors
      .filter((x) => x.gender === "female")
      .map(({ name }) => name.trim())
      .sort(),
    labels: first.categories.map(({ name }) => name.trim()).sort(),
    studio: first.studio_name.trim(),
    thumbnail,
  };
};

handler.requiredVersion = "0.30.0-rc.3 - 1";

applyMetadata(handler, info);

export default handler;
