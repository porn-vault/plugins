import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    testTimeout: 15_000,
    include: ["plugins/**/*.{spec,test}.{js,ts}"],
  },
});
