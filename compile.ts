import { readdirSync } from "node:fs";
import { resolve } from "node:path";

import esbuild from "esbuild";

(async () => {
  const pluginsFolder = "plugins";
  const outputDir = "dist";

  for (const pluginName of readdirSync(pluginsFolder)) {
    const inputFile = resolve(pluginsFolder, pluginName, "main.js");
    const outputFile = resolve(outputDir, `${pluginName}.mjs`);

    console.log(`Bundling ${pluginName}`);

    const bundle = await esbuild.build({
      entryPoints: [inputFile],
      outfile: outputFile,
      bundle: true,
      platform: "node",
      format: "esm",
    });

    if (bundle.errors.length) {
      console.log("Errors:")
      console.log(bundle.errors);
      process.exit(1);
    }
    if (bundle.warnings.length) {
      console.log("Warnings:");
      console.log(bundle.warnings);
    }
  }

  console.log("Build done");
  process.exit(0);
})();
